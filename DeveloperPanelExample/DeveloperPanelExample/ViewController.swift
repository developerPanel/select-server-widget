//
//  ViewController.swift
//  DeveloperPanelExample
//
//  Created by Dmitry Letko on 12/9/16.
//  Copyright © 2016 Intellectsoft LLC. All rights reserved.
//

import UIKit
import DeveloperPanel

class ViewController: UIViewController {
    fileprivate lazy var widget = Widget(MyConfigManager.shared, actions: MyAction.createCustomActions())
    
    @IBOutlet weak var configurationLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        infoLabel.text = "Swipe up with 3 finger to view widget settings"
        
        updateConfigurationLabel()
    }
    
    public func updateConfigurationLabel() {
        configurationLabel.text = configurationString()
    }

    internal func configurationString() -> String {
        guard let endpoint = MyConfigManager.shared.selectedConfiguration?.endpoint else { return "Nothing" }
        guard let title = MyConfigManager.shared.selectedConfiguration?.title else { return "Nothing" }
        
        return "Title: \(title) \nEndpoint: \(endpoint)"
    }
    
    @IBAction func handleOpenWidgetButton(sender: UIButton) {
        widget.open()
    }
}

