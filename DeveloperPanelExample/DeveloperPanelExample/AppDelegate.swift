//
//  AppDelegate.swift
//  DeveloperPanelExample
//
//  Created by Dmitry Letko on 12/9/16.
//  Copyright © 2016 Intellectsoft LLC. All rights reserved.
//

import UIKit
import DeveloperPanel


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow? {
        didSet {
            window?.attach(widget: Widget(MyConfigManager.shared, actions: MyAction.createCustomActions()))
        }
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        MyConfigManager.shared.output = self
        
        return true
    }
}

extension AppDelegate: ConfigurationManagerOutput {
    func configurationManager<MyConfig>(_ configurationManager: ConfigurationManager<MyConfig>, didSelectConfiguration configuration: MyConfig) -> Bool {
        guard let vc = window?.rootViewController as? ViewController else { return true }
        vc.updateConfigurationLabel()
        
        return true
    }
}
