//
//  MyConfig.swift
//  DeveloperPanelExample
//
//  Created by Dmitry Letko on 12/9/16.
//  Copyright © 2016 Intellectsoft LLC. All rights reserved.
//

import DeveloperPanel
import UIKit

struct MyConfig: Configuration {    
    let title: String
    let endpoint: String
    
    public var uid: String {
        return endpoint
    }
}

extension MyConfig: WidgetConfiguration {
    var description: String? {
        return endpoint
    }
}

extension MyConfig {
    static func configurations() -> [MyConfig] {
        return [MyConfig(title: "dev1", endpoint: "asd1"),
                MyConfig(title: "dev2", endpoint: "asd2"),
                MyConfig(title: "dev3", endpoint: "asd3")]
    }
}

class MyConfigManager: ConfigurationManager<MyConfig> {
    static let shared = MyConfigManager(configurations: MyConfig.configurations())
}

public typealias actionBlock = ()->()

struct MyAction {
    let title: String
    let description: String?
    let action: actionBlock?
    
    static func createCustomActions() -> [WidgetAction]? {
        let myCustomActionBlock: actionBlock = {
            let alertController = UIAlertController(title: "Custom action", message: "This is action", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            
            let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window!.rootViewController?.present(alertController, animated: true, completion: nil)
        }
        let myCustomAction = MyAction(title: "Alert Action", description: "", action: myCustomActionBlock)
        
        return [myCustomAction]
    }
}

extension MyAction: WidgetAction {
    func perform() {
        action?()
    }
}
