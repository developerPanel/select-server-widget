//
//  ConfigurationManager.swift
//  DeveloperPanel
//
//  Created by Dmitry Letko on 12/9/16.
//  Copyright © 2016 Intellectsoft LLC. All rights reserved.
//

import Foundation
import KeychainAccess

public protocol Configuration: Equatable {
    var uid: String { get }
}

public extension Configuration {
    static func ==(lhs: Self, rhs: Self) -> Bool {
        return lhs.uid == rhs.uid
    }
}

public protocol ConfigurationManagerOutput: AnyObject {
    /*
     *  -   Returns:    Return `true` if normal execution of the application should continue,
     *                  otherwise it will be aborted.
     */
    func configurationManager<T: Configuration>(_ configurationManager: ConfigurationManager<T>, didSelectConfiguration configuration: T) -> Bool
}

open class ConfigurationManager<T: Configuration> {
    fileprivate var manuallySelectedConfiguration: T?
    fileprivate let storage: Storage
    
    public let configurations: [T]
    public weak var output: ConfigurationManagerOutput?
    
    public init(configurations: [T], storage: Storage) {
        self.configurations = configurations
        self.storage = storage
        
        if let selectedConfigurationUid = storage.string(forKey: StorageKeys.selectedConfiguration) {
            if let indexOfSelectedConfiguration = configurations.index(where: { (configuration) -> Bool in
                return configuration.uid == selectedConfigurationUid
            }) {
                manuallySelectedConfiguration = configurations[indexOfSelectedConfiguration]
            }
        }
    }
    
    public var selectedConfiguration: T? {
        if let selectedConfiguration = manuallySelectedConfiguration {
            return selectedConfiguration
        }
        
        if let selectedConfiguration = configurations.first {
            return selectedConfiguration
        }
        
        return nil
    }
    
    public func selectConfiguration(_ configuration: T) {
        assert(configurations.index(of: configuration) != nil)
        
        if manuallySelectedConfiguration != configuration {
            manuallySelectedConfiguration = configuration
            
            do {
                try storage.set(string: configuration.uid, forKey: StorageKeys.selectedConfiguration)
            } catch {
                fatalError(error.localizedDescription)
            }
            
            if let outputNonNil = output {
                if outputNonNil.configurationManager(self, didSelectConfiguration: configuration) == false {
                    abort()
                }
            } else {
                abort()
            }
        }
    }
}

public extension ConfigurationManager {
    public convenience init(configurations: [T]) {
        self.init(configurations: configurations, useKeychain: true, name: "default")
    }
    
    public convenience init(configurations: [T], useKeychain: Bool, name: String) {
        let storage: Storage = {
            if useKeychain == true {
                return Keychain(service: "net.intellectsoft.DeveloperPanel." + name)
            } else {
                return UserDefaults(suiteName: "net.intellectsoft.DeveloperPanel." + name)!
            }
        }()
        
        self.init(configurations: configurations, storage: storage)
    }
}
