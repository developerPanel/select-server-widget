//
//  Storage.swift
//  DeveloperPanel
//
//  Created by Dmitry Letko on 1/9/17.
//  Copyright © 2017 Intellectsoft LLC. All rights reserved.
//

import Foundation
import KeychainAccess

public struct StorageKeys {
    static let selectedConfiguration = "selected"
}

public protocol Storage {
    func set(string: String?, forKey key: String) throws
    func string(forKey key: String) -> String?
}

extension UserDefaults: Storage {
    public func set(string: String?, forKey key: String) throws {
        set(string, forKey: key)
        synchronize()
    }
}

extension Keychain: Storage {
    public func set(string: String?, forKey key: String) throws {
        if let string = string {
            try set(string, key: key)
        } else {
            try remove(key)
        }
    }
    
    public func string(forKey key: String) -> String? {
        return (try? get(key)) ?? nil
    }
}
