//
//  WidgetViewController.swift
//  DeveloperPanel
//
//  Created by Dmitry Letko on 12/14/16.
//  Copyright © 2016 Intellectsoft LLC. All rights reserved.
//

import UIKit

struct WidgetViewRow {
    let title: String?
    let description: String?
}

struct WidgetViewSection {
    var title: String?
    var rows: [WidgetViewRow]?
}

extension WidgetViewRow: Equatable {
    public static func ==(lhs: WidgetViewRow, rhs: WidgetViewRow) -> Bool {
        return lhs.title == rhs.title && lhs.description == rhs.description
    }
}

extension WidgetViewRow: Hashable {
    public var hashValue: Int {
        return (title?.hashValue ?? 0) ^ (description?.hashValue ?? 0)
    }
}

protocol WidgetViewInput: AnyObject {
    func show(sections: [WidgetViewSection]?)
    func select(row: WidgetViewRow?)
    func enable(save enable: Bool)
}

protocol WidgetViewOutput {
    func loadContent()
    func didSelect(row: WidgetViewRow)
    func cancel()
    func save()
}

class WidgetViewCell: UITableViewCell {
    static let reuseIdentifier = "WidgetViewCell"
}

class WidgetViewController: UITableViewController {
    @IBOutlet var saveBarButtonItem: UIBarButtonItem?
    
    fileprivate var sections: [WidgetViewSection]?

    var output: WidgetViewOutput?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        clearsSelectionOnViewWillAppear = false
        output?.loadContent()
    }
    
    func row(at indexPath: IndexPath) -> WidgetViewRow? {
        return sections?[indexPath.section].rows?[indexPath.row]
    }
}

extension WidgetViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return sections?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections?[section].rows?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections?[section].title
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: WidgetViewCell.reuseIdentifier, for: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let row = row(at: indexPath) {
            cell.textLabel?.text = row.title
            cell.detailTextLabel?.text = row.description
        }
    }
}

extension WidgetViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let row = row(at: indexPath) {
            output?.didSelect(row: row)
        }
    }
}

extension WidgetViewController {
    @IBAction func handleCancelBarButtonItem(_ sender: UIBarButtonItem) {
        output?.cancel()
    }
    
    @IBAction func handleSaveBarButtonItem(_ sender: UIBarButtonItem) {
        output?.save()
    }
}

extension WidgetViewController: WidgetViewInput {
    func show(sections: [WidgetViewSection]?) {
        self.sections = sections
        
        tableView.reloadData()
    }
    
    func select(row: WidgetViewRow?) {
        let indexPath: IndexPath? = {
            if let rowNotNil = row, let enumerated = sections?.enumerated() {
                for (indexOfSection, section) in enumerated {
                    if let indexOfRow = section.rows?.index(of: rowNotNil) {
                        return IndexPath(row: indexOfRow, section: indexOfSection)
                    }
                }
            }
            
            return nil;
        }()
        
        tableView.selectRow(at: indexPath,
                            animated: false,
                            scrollPosition: .none)
    }
    
    func enable(save enable: Bool) {
        saveBarButtonItem?.isEnabled = enable
    }
}
