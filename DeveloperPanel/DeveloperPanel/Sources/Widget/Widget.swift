//
//  Widget.swift
//  DeveloperPanel
//
//  Created by Dmitry Letko on 12/13/16.
//  Copyright © 2016 Intellectsoft LLC. All rights reserved.
//

import Foundation
import UIKit

public protocol WidgetConfiguration: Configuration {
    var title: String { get }
    var description: String? { get }
}

public protocol WidgetAction {
    var title: String { get }
    var description: String? { get }
    
    func perform()
}

protocol WidgetRouterInput: AnyObject {
    func close()
}

class WidgetGestureRecognizer: UISwipeGestureRecognizer {
    var widget: AnyObject?
    
    override init(target: Any?, action: Selector?) {
        super.init(target: target, action: action)
        
        direction = .up
        numberOfTouchesRequired = 3
    }
}

public class Widget<T: WidgetConfiguration>: WidgetRouterInput {
    fileprivate let configurationManager: ConfigurationManager<T>
    fileprivate var window: UIWindow?
    fileprivate var actions: [WidgetAction]?
    fileprivate weak var oldKeyWindow: UIWindow?
    fileprivate weak var gestureRecognizer: WidgetGestureRecognizer? {
        didSet(oldGestureRecognizer) {
            if oldGestureRecognizer?.widget === self {
                oldGestureRecognizer?.widget = nil
            }
            
            if let newGestureRecognizer = gestureRecognizer {
                newGestureRecognizer.widget = self
            }
        }
    }
    
    public init(_ configurationManager: ConfigurationManager<T>, actions: [WidgetAction]? = nil) {
        self.configurationManager = configurationManager
        self.actions = actions
    }
    
    public var isAttached: Bool {
        return gestureRecognizer != nil
    }
    
    /**
     *  The convenience method to instantiate default gesture recognizer, add it to specified window and
     *  link its handler to opening of the widget.
     */
    public func attach(toWindow window: UIWindow) {
        assert(isAttached == false, "Widget is already attached to some window.")
        assert(Thread.isMainThread, "The method is suposed to be called in the main thread only.")
        
        let gestureRecognizer = WidgetGestureRecognizer(target: self, action: #selector(handleGesture(_:)))
        
        window.addGestureRecognizer(gestureRecognizer)
        
        self.gestureRecognizer = gestureRecognizer
    }
    
    public var isOpened: Bool {
        return window != nil
    }
    
    public func open() {
        assert(isOpened == false, "The widget is already shown.")
        assert(Thread.isMainThread == true, "The method is suposed to be called in the main thread only.")
        
        let storyboard = UIStoryboard(name: "Widget", bundle: Bundle(for: WidgetViewController.self))
        let navigationController = storyboard.instantiateInitialViewController() as! UINavigationController
        let viewController = navigationController.topViewController as! WidgetViewController
        let presenter = WidgetPresenter(configurationManager, actions: actions)
        
        viewController.output = presenter
        presenter.view = viewController
        presenter.router = self
        
        oldKeyWindow = UIApplication.shared.keyWindow
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
    }
    
    func close() {
        assert(Thread.isMainThread, "The method is suposed to be called in the main thread only.")
        
        window?.isHidden = false
        window = nil
        oldKeyWindow?.makeKeyAndVisible()
        oldKeyWindow = nil
    }
    
    @objc private func handleGesture(_ sender: WidgetGestureRecognizer) {
        if sender.state == .recognized {
            open()
        }
    }
}

public extension UIWindow {
    public func attach<T: WidgetConfiguration>(widget: Widget<T>) {
        widget.attach(toWindow: self)
    }
}
