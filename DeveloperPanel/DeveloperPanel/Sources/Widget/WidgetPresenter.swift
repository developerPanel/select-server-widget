//
//  WidgetPresenter.swift
//  DeveloperPanel
//
//  Created by Dmitry Letko on 12/14/16.
//  Copyright © 2016 Intellectsoft LLC. All rights reserved.
//

import Foundation

extension WidgetViewRow {
    init<T: WidgetConfiguration>(configuration: T) {
        self.init(title: configuration.title, description: configuration.description)
    }
    
    init(action: WidgetAction) {
        self.init(title: action.title, description: action.description)
    }
}

class WidgetPresenter<T: WidgetConfiguration> {
    fileprivate let configurationManager: ConfigurationManager<T>
    fileprivate let actions: [WidgetAction]?
    fileprivate var actionsByRow = [WidgetViewRow: WidgetAction]()
    fileprivate var configurationsByRow = [WidgetViewRow: T]()
    fileprivate var configurationToSelect: T?
    
    weak var router: WidgetRouterInput?
    weak var view: WidgetViewInput?
    
    init(_ configurationManager: ConfigurationManager<T>, actions: [WidgetAction]? = nil) {
        self.configurationManager = configurationManager
        self.actions = actions
    }
}

extension WidgetPresenter: WidgetViewOutput {
    func loadContent() {
        var sections: [WidgetViewSection] = {
            var section = WidgetViewSection()
            section.title = "Configurations"
            section.rows = configurationManager.configurations.flatMap({ (configuration) -> WidgetViewRow in
                let row = WidgetViewRow(configuration: configuration)
                
                configurationsByRow[row] = configuration
                
                return row
            })
        
            return [section]
        }()
        
        if let actions = actions {
            var section = WidgetViewSection()
            section.title = "Custom Actions"
            section.rows = actions.flatMap({ (action) -> WidgetViewRow in
                let row = WidgetViewRow(action: action)
                
                actionsByRow[row] = action
                
                return row
            })
            
            sections.append(section)
        }
        
        view?.enable(save: false)
        view?.show(sections: sections)
        
        if let selectedConfiguration = configurationManager.selectedConfiguration {
            view?.select(row: WidgetViewRow(configuration: selectedConfiguration))
        }
    }
    
    func didSelect(row: WidgetViewRow) {
        if let configuration = configurationsByRow[row] {
            configurationToSelect = configuration
            view?.enable(save: (configurationManager.selectedConfiguration != configuration))
        } else if let action = actionsByRow[row] {
            router?.close()
            action.perform()
        }
    }
    
    func cancel() {
        router?.close()
    }
    
    func save() {
        configurationManager.selectConfiguration(configurationToSelect!)
        router?.close()
    }    
}
