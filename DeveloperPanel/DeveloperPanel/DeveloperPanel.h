//
//  DeveloperPanel.h
//  DeveloperPanel
//
//  Created by Dmitry Letko on 12/9/16.
//  Copyright © 2016 Intellectsoft LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for DeveloperPanel.
FOUNDATION_EXPORT double DeveloperPanelVersionNumber;

//! Project version string for DeveloperPanel.
FOUNDATION_EXPORT const unsigned char DeveloperPanelVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <DeveloperPanel/PublicHeader.h>

