Pod::Spec.new do |s|
  s.name                    = 'DeveloperPanel'
  s.version                 = '0.1.2'
  s.license                 = { :type => 'MIT', :file => 'LICENSE' }
  s.summary                 = 'Meaningful description of DeveloperPanel.'
  s.homepage                = 'https://gitlab.isdev.info/ios-team/select-server-widget'
  s.authors                 = { 'Dmitry Letko' => 'dmitry.letko@intellectsoft.net', 'Anton Kostko' => 'anton.kostko@intellectsoft.net' }
  s.source                  = { :git => 'https://gitlab.isdev.info/ios-team/select-server-widget.git', :tag => s.version }
# s.source_files            = 'DeveloperPanel/DeveloperPanel/Sources/Utilities/**/*.swift'
  s.platform                = :ios, '9.0'
  s.default_subspec         = 'Manager'
  
  s.subspec 'Manager' do |ss|
    ss.source_files         = 'DeveloperPanel/DeveloperPanel/Sources/Manager/**/*.swift'
    
    ss.dependency 'KeychainAccess'
  end
                       
  s.subspec 'Widget' do |ss|
    ss.source_files         = 'DeveloperPanel/DeveloperPanel/Sources/Widget/**/*.swift'
    ss.resources            = 'DeveloperPanel/DeveloperPanel/Sources/Widget/**/*.storyboard'
    ss.frameworks           = 'UIKit'
    ss.user_target_xcconfig = {
      'OTHER_SWIFT_FLAGS' => '-D DEVELOPER_PANEL_WIDGET_ENABLED',
    }
    
    ss.dependency 'DeveloperPanel/Manager'
  end
  
# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC
                       
end
